from labs.ptoop1.drawers.drawer import Drawer


class LineDrawer(Drawer):

    def __init__(self, shape):
        Drawer.__init__(self, shape)

    def draw(self, turtle):
        self.moveTurtle(turtle, self.shape.x1, self.shape.y2)
        turtle.goto(self.shape.x2, self.shape.y2)
        self.moveTurtle(turtle)
