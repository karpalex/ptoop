from labs.ptoop1.drawers.drawer import Drawer


class TriangleDrawer(Drawer):

    def __init__(self, shape):
        Drawer.__init__(self, shape)

    def draw(self, turtle):
        self.moveTurtle(turtle, self.shape.x, self.shape.y)

        turtle.left(self.shape.rotate)

        turtle.forward(self.shape.b)
        pos_b = turtle.pos()

        self.moveTurtle(turtle, self.shape.x, self.shape.y)
        turtle.right(180 - self.shape.alpha)
        turtle.forward(self.shape.c)
        turtle.goto(pos_b)

        self.moveTurtle(turtle)
