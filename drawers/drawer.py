class Drawer():

    shape = None

    def __init__(self, shape):
        self.shape = shape

    def moveTurtle(self, turtle, x=0, y=0):
        turtle.penup()
        turtle.goto(x, y)
        turtle.pendown()