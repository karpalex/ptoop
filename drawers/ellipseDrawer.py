from labs.ptoop1.drawers.drawer import Drawer


class EllipseDrawer(Drawer):

    def __init__(self, shape):
        Drawer.__init__(self, shape)

    def draw(self, turtle):
        self.moveTurtle(turtle, self.shape.x, self.shape.y)

        turtle.left(self.shape.rotate)

        for i in range(2):
            turtle.circle(self.shape.radius1, 90)
            turtle.circle(self.shape.radius2, 90)

        self.moveTurtle(turtle)
