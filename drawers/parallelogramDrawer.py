from labs.ptoop1.drawers.drawer import Drawer


class ParallelogramDrawer(Drawer):

    def __init__(self, shape):
        Drawer.__init__(self, shape)

    def draw(self, turtle):
        self.moveTurtle(turtle, self.shape.x, self.shape.y)
        turtle.left(self.shape.rotate)

        for i in range(2):
            turtle.forward(self.shape.width)
            turtle.left(self.shape.alpha)
            turtle.forward(self.shape.height)
            turtle.left(180 - self.shape.alpha)

        self.moveTurtle(turtle)
