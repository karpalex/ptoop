from labs.ptoop1.shapes.shape import Shape


class Line(Shape):

    def __init__(self, x1=0, y1=0, x2=0, y2=0):
        Shape.__init__(self)
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2

