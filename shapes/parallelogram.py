from labs.ptoop1.shapes.shape import Shape


class Parallelogram(Shape):

    def __init__(self, x = 0, y=0, w=0, h=0, alpha=0, rotate=0):
        Shape.__init__(self)
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        self.rotate = rotate

        # if alpha = 90 it's rectangle (and if w = h it's square)

        self.alpha = alpha


