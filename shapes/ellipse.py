from labs.ptoop1.shapes.shape import Shape


class Ellipse(Shape):


    def __init__(self, x=0, y=0, r1=0, r2=0, rotate=0):
        Shape.__init__(self)
        self.x = x
        self.y = y

        # if radius1 = radius2 it's circle

        self.radius1 = r1
        self.radius2 = r2
        self.rotate = rotate



