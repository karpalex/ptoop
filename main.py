# from turtle import Turtle
#
# from labs.ptoop1.drawers.parallelogramDrawer import ParallelogramDrawer
# from labs.ptoop1.drawers.triangleDrawer import TriangleDrawer
# from labs.ptoop1.shapes.parallelogram import Parallelogram
# from labs.ptoop1.shapes.triangle import Triangle
#

# from click._compat import raw_input
#
# animals = ['Lion', 'Tiger', 'Fish']
# favourite = raw_input('Please, choose the shape type: (' + ' or '.join(animals) + '):')
#
# print(favourite)

import importlib
from turtle import Turtle

from click._compat import raw_input

shapes_module = 'labs.ptoop1.shapes.'
drawers_module = 'labs.ptoop1.drawers.'

shapes = ['ellipse', 'line', 'parallelogram', 'triangle']

turtle = Turtle()  # drawing library object
turtle.speed(0)  # remove standart animation
turtle.ht()  # remove standart style


while True:


    shape = raw_input('Please, choose the shape type: (' + ' or '.join(shapes) + '):')

    module = importlib.import_module(shapes_module + shape)
    class_ = getattr(module, shape.capitalize())
    instance = class_()

    for field, val in instance.__dict__.items():
        value =  int(raw_input(field + ':'))
        setattr(instance, field, value)

    drawer = shape + 'Drawer'
    drawer_module = importlib.import_module(drawers_module + drawer)
    drawer_class_ = getattr(drawer_module,  shape.capitalize() + "Drawer")
    drawer_instance = drawer_class_(instance)

    drawer_instance.draw(turtle)




