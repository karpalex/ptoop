import importlib

from lxml import objectify

from ptoop.shapes.shape import Shape

xml = '''<?xml version="1.0" encoding="UTF-8"?>'''

shapes_module = 'labs.ptoop1.shapes.'

class Serializer():

    def read(self, filename):
        try:
            f = open(filename, "r")
            object = objectify.fromstring(f.read())

            module = importlib.import_module(shapes_module + object.name)
            class_ = getattr(module, object.name.capitalize())
            shape = class_()

            for field, val in shape.__dict__.items():
                setattr(shape, field, object[val])

            f.close()
            return shape

        except:
            return None




    def write(self, filename, shape):
        root = objectify.fromstring(xml)
        shape.name = shape.__name__
        root.append(shape)

        f = open(filename, "w+")

        f.write(root.tostring())
        f.close()




